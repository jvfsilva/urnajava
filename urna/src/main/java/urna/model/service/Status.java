package urna.model.service;

public enum Status {
	NAOINICIADO(0),
	SENADOR1(1),
	SENADOR2(2),
	PRESIDENTE(3),
	FINALIZADO(4);
	
	Status(int i) {}
}
