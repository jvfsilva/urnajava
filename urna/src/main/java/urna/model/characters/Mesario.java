package urna.model.characters;
import java.util.Arrays;  

public class Mesario extends Individuo{
	private char[] senha;
	
	public Mesario (Individuo i, char[] pwd) {
		super( i.nome, i.cpf, i.telefone, i.endereco);
		this.senha = pwd;
	}
	
	public boolean conferirSenha(char[] s) {
		if (Arrays.equals(this.senha, s))
			return true;
		else
			return false;
	}
}
