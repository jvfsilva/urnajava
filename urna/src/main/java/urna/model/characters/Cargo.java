package urna.model.characters;

public enum Cargo {
	SENADOR("senador"),
	PRESIDENTE("presidente");
	
	private String descricao;
	
	Cargo(String d) {
		this.descricao = d;
	}
	
	public String getDescricao() {
        return descricao;
    }
}
