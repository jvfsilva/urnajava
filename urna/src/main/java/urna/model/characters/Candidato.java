package urna.model.characters;

public class Candidato extends Individuo {
	private int numero;
	private String partido;
	private Cargo cargo;
	private String localidade;
	private int votosRecebidos;
	private double porcentagemFinal;
	private int posicaoFinal;
	
	public Candidato(Individuo i, int num, String p, Cargo c, String l) {
		super( i.nome, i.cpf, i.telefone, i.endereco);
		this.numero = num;
		this.partido = p;
		this.cargo = c;
		this.localidade = l;
		this.votosRecebidos = 0;
		this.porcentagemFinal = 0;
		this.posicaoFinal = 0;
	}
	
	public int getNumero() { return this.numero; }
	
	public String getPartido() { return this.partido; }
	
	public Cargo getCargo() { return this.cargo; }
	
	public String getLocalidade() { return this.localidade; } 
}
