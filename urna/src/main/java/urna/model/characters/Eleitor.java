package urna.model.characters;

public class Eleitor extends Individuo{
	
	private int id;
	private int[] registroEleitoral;
	private int sessao;
	private int zona;
	private boolean votoRealizado;
	
	public Eleitor(int id, Individuo i, int[] registro, int sessao, int zona) {
		super( i.nome, i.cpf, i.telefone, i.endereco);
		this.id = id;
		this.registroEleitoral = registro;
		this.sessao = sessao;
		this.zona = zona;
		this.votoRealizado = false;
	}
	
	public int[] getRegistro() { return this.registroEleitoral; }
	
	public int getSessao() { return this.sessao; }
	
	public int getZona() { return this.zona; }

}
