<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>

<html>
    <head>
        <title>VotaÃ§Ã£o</title>
        <meta charset="UTF-8">
        <script src="codigo.js"></script>
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container">
            <div class="branco">
                <div class="cinzaClaro">
                    <p>NÃºmero do primeiro candidato(a) para senador(a):</p>
                    <form action="<c:url value= "primeirosenador/primeiroSenador"/>">
                        <input name="primeiroDigito" size="1" id="1campo1" value="" maxlength="1" type="text" readonly="readonly"/>
                        <input name="segundoDigito" size="1" id="1campo2" value="" maxlength="1" type="text" readonly="readonly"/>
                        <input name="terceiroDigito" size="1" id="1campo3" value="" maxlength="1" type="text" readonly="readonly"/>
                    </form>
                </div>
                <div class="cinzaEscuro">
                    <b>JUSTIÃA<br>ELEITORAL UFMG</b> 
                </div>
                <div class="preto">
                    <button class="bnt click" onclick="inserir(1,1)">1</button>
                    <button class="bnt click" onclick="inserir(2,1)">2</button>
                    <button class="bnt click" onclick="inserir(3,1)">3</button>
                    <button class="bnt click" onclick="inserir(4,1)">4</button>
                    <button class="bnt click" onclick="inserir(5,1)">5</button>
                    <button class="bnt click" onclick="inserir(6,1)">6</button>
                    <button class="bnt click" onclick="inserir(7,1)">7</button>
                    <button class="bnt click" onclick="inserir(8,1)">8</button>
                    <button class="bnt click" onclick="inserir(9,1)">9</button>
                    <button class="bnt click" onclick="inserir(0,1)">0</button>
                    <div class="teclado2">
                        <button class="branco  click">BRANCO</button>
                        <button class="laranja  click" onclick="corrige(1)">CORRIGE</button>
                        <button type="submit" href="<c:url value="segundoSenador"/>" class="verde" onclick="votar(1)">CONFIRMA</button>
                    </div>
                </div>
            </div>
            <a href="resultado.html">Resultado</a>
        </div> 
       
    </body>
</html>