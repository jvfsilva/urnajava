package urna.model.dao;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import java.io.BufferedWriter;
import java.io.File; 
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Generic {
	private static String base;
	
	public Generic (String b) {
		Generic.base =  b;
	}
	
	public boolean salvar(String arquivo, String msg) {
		try {
			BufferedWriter arqW = new BufferedWriter(new FileWriter(base + arquivo, true));
			arqW.append(msg + "\n");
			arqW.close();
			//System.out.println("Salvo no arquivo " + arquivo + " com sucesso.");
			return true;
	    } catch (IOException e) {
	    	System.out.println("Ocorreu um erro ao tentar salvar no arquivo.");
	    	e.printStackTrace();
	    	return false;
	    }
	}

	public String ler(String arquivo) {
		try {
			File arq = new File(base + arquivo);
			Scanner arqR = new Scanner(arq);
			String data = "";
			while (arqR.hasNextLine()) {
				String tmp = arqR.nextLine();
				data += (tmp + "\n"); 
				//System.out.println(tmp);
			}
			arqR.close();
			return data;
	    } catch (FileNotFoundException e) {
	    	System.out.println("Ocorreu um erro ao tentar ler do arquivo.");
	    	e.printStackTrace();
	    	return "";
	    }
	}
	
	// TODO: h� problemas nesta fun��o
	public String ler(String arquivo, String regex) {
		try {
			File arq = new File(base + arquivo);
			
			Scanner arqR = new Scanner(arq);
			Pattern p = Pattern.compile(regex);
			String data = "";
			while (arqR.hasNextLine()) {
				String tmp = arqR.nextLine();
				data += (tmp + "\n"); 
			}
			Matcher match = p.matcher(data);
			arqR.close();
			return match.group();
	    } catch (FileNotFoundException e) {
	    	System.out.println("Ocorreu um erro ao tentar ler do arquivo.");
	    	e.printStackTrace();
	    	return "";
	    }
	}
	
	public List<String[]> lerCsv(String arquivo)  {
		try (
            CSVReader csvReader = new CSVReader(new FileReader(base + arquivo));
        ) {
			String[] line;
			List<String[]> lines = new LinkedList<String[]>();
            while ((line = csvReader.readNext()) != null) {
                lines.add(line);
             }
            return lines;
        }
		catch ( Exception  e) {
			System.out.println("Ocorreu um erro ao buscar no registro.");
			e.printStackTrace();
			return null;
	    }
	}
	
	public void escreverCsv(String arquivo, String[] linha) {
		try {
			CSVWriter writer = new CSVWriter(new FileWriter(base + arquivo, true));
			writer.writeNext(linha);
		    writer.flush();
		    //System.out.println("Nova linha inserida no CSV");
		    writer.close();
		}
		catch (IOException e) {
			System.out.println("Ocorreu um erro ao tentar escrever no arquivo CSV.");
			e.printStackTrace();
	    }    
	}
	
	
	public boolean novoRegistro(String nome) {
		try {
			File registro = new File(base + nome);
	        if (registro.createNewFile()) {
	        	//System.out.println("Registro criado: " + registro.getName());
	        	return true;
	        } else {
	        	//System.out.println("Registro existente.");
	        	return false;
	        }
		}
		catch (IOException e) {
			System.out.println("Ocorreu um erro ao tentar criar novo registro.");
			e.printStackTrace();
			return false;
	    }
	}
	
	public boolean deletarRegistro(String nome) {
		File arq = new File(base + nome); 
	    if (arq.delete()) { 
	    	//System.out.println("Removeu o arquivo: " + arq.getName());
	    	return true;
	    } else {
	    	System.out.println("Falhou ao tentar deletar o arquivo.");
	    	return false;
	    } 
	}
	
	public boolean atualizar(String arquivo, String localizacao, String msg) {
		// TODO
		return false;
	}
}
