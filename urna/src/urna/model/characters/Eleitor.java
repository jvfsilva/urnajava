package urna.model.characters;

public class Eleitor extends Individuo{
	
	private int id;
	private int[] registroEleitoral;
	private int sessao;
	private int zona;
	private boolean votoRealizado;
	
	public Eleitor(int id, Individuo ind, int[] registro, int sessao, int zona) {
		this.id = id;
		this.Individuo = ind;
		this.registroEleitoral = registro;
		this.sessao = sessao;
		this.zona = zona;
		this.votoRealizado = false;
	}
	
	public int[] getRegistro() { return this.registroEleitoral; }
	
	public int getsessao() { return this.sessao; }
	
	public int getzona() { return this.zona; }

}
