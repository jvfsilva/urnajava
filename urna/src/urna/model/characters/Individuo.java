package urna.model.characters;

public class Individuo {
	protected String nome;
	protected int[] cpf;
	protected int[] telefone;
	protected String endereco;
	
	public Individuo (String nome, int[] cpf, int[] telefone, String endereco) {
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.endereco = endereco;
	}
	
	public int[] consultarcpf() {
		return this.cpf;
	}
	
	public int[] gettelefone() { return this.telefone; }
	
	public String getendereco() { return this.endereco; }
	
	public boolean atualizarendereco(String novo) {
		if (novo != null && novo != "") {
			this.endereco = novo;
			//System.out.println("endereco atualizado com sucesso");
			return true;
		}
			
		else {
			//System.out.println("Novo endereco invalido ...");
			return false;
		}
	}
	
	public boolean atualizartelefone(int[] novo) {
		if (novo != null && novo.length > 8) {
			this.telefone = novo;
			//System.out.println("telefone atualizado com sucesso");
			return true;
		}
		else {
			//System.out.println("Novo telefone invalido ...");
			return false;
		}
	}

}
