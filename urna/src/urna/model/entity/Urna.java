package urna.model.entity;

import java.util.LinkedList;
import java.util.List;

import urna.model.dao.Generic;

public class Urna {
	private int id;
	private int sessao;
	private int zona;
	private Generic dao;
	private List<VotoConsolidado> votos;
	
	private static final String path = System.getProperty("user.dir") + "/registros/";
	private static final String regVotos = "votos.csv";
	private static final String logs = "logs.txt";
	
	public Urna(int s, int z) {
		this.id = (int)Math.random() * 10;
		this.sessao = s;
		this.zona = z;
		this.dao = new Generic(path);
		this.votos = null;
		
		dao.novoRegistro(regVotos);
		dao.novoRegistro(logs);
		
		dao.salvar(logs, "Inicializando urna eletr�nica ...");
		dao.salvar(logs, "Dados da Urna -> ID: " + this.id + ", Zona: " + this.zona + ", Sess�o: " + this.sessao);
	}
	
	public int getSessao() { return this.sessao; }
	
	public int getZona() { return this.zona; }
	
	public void registrarVoto(Voto v) {
		String linha[] = v.votoFinal();
		dao.escreverCsv(regVotos, linha);
		dao.salvar(logs, "Voto ID " + v.id + " registrado.");
	}
	
	public void imprimirVotos() {
		dao.salvar(logs, "Solicitada a impress�o dos votos.");
		List<String[]> registros = dao.lerCsv(regVotos);
		registros.forEach(r -> {
			for (int j = 0; j < r.length; j++)
				System.out.print(r[j] + " ");
			System.out.println("");
		});
	}
	
	public void consolidarResultados() {
		dao.salvar(logs, "Solicitada a consolida��o dos resultados.");
		List<VotoConsolidado> votos = new LinkedList<VotoConsolidado>();
		List<String[]> registros = dao.lerCsv(regVotos);
		registros.forEach(r -> {
			votos.add(new VotoConsolidado(r[0], r[1], r[2], r[3]));
		});
		this.votos = votos;
	}
}
