package urna.model.entity;

import urna.model.characters.*;
import urna.model.service.Status;
import urna.model.service.Votacao;

import java.util.Date;

public class Voto {
	private int id ;
	private Date data; 
	private Candidato primeiroSenador;
	private Candidato segundoSenador;
	private Candidato presidente; 
	private Status status;
	
	public Voto(Votacao vot, Candidato senador1, Candidato senador2, Candidato presidente) {
		this.id = vot.getVotosTotais() + 1;
		this.primeiroSenador = senador1;
		this.segundoSenador = senador2;
		this.presidente = presidente;
		this.data = new Date();
		this.status = Status.NAOINICIADO;
	}
	
	public Status getStatus() {return this.status; }
	
	public String[] votoFinal() {
		String[] voto = { Integer.toString(this.id),
				Integer.toString(this.primeiroSenador.getNumero()), 
	        	Integer.toString(this.segundoSenador.getNumero()), 
	        	Integer.toString(this.presidente.getNumero())};
        return voto;
    }
	
	public void setPrimeiroSenador(Candidato c, boolean votoNulo) {
		if (!votoNulo) {
			if (c.getCargo() == Cargo.SENADOR)
				this.primeiroSenador = c;
			else
				this.primeiroSenador = null;
		}
		else {
			this.primeiroSenador = null;
		}
	}
	
	public void setSegundoSenador(Candidato c, boolean votoNulo) {
		if (!votoNulo) {
			if (c.getCargo() == Cargo.SENADOR)
				this.segundoSenador = c;
			else
				this.segundoSenador = null;
		}
		else {
			this.segundoSenador = null;
		}
	}
	
	public void setPresidente(Candidato c, boolean votoNulo) {
		if (!votoNulo) {
			if (c.getCargo() == Cargo.PRESIDENTE)
				this.presidente = c;
			else
				this.presidente = null;
		}
		else {
			this.presidente = null;
		}
	}
	
	public void finalizar (Urna u) {
		u.registrarVoto(this);
	}
}
