package urna.model.service;

import urna.model.entity.*;
import urna.model.characters.*;
import urna.model.dao.Generic;
import java.util.*;

public class Votacao {
	
	private Urna urna; 
	private Mesario mesario;
	private List<Candidato> candidatos;
	private List<Eleitor> eleitoresAptos;
	private List<Eleitor> eleitoresVotantes;
	private Generic dao;
	
	private static final String path = System.getProperty("user.dir") + "/registros/";
	private static final String logs = "logs.txt";
	
	public Votacao(Urna u, Mesario m, List<Candidato> cs, List<Eleitor> ea) {
		this.urna = u;
		this.mesario = m;
		this.candidatos = cs;
		this.eleitoresAptos = ea;
		this.dao = new Generic(path);
		
		dao.salvar(logs, "Vota��o sendo iniciada ...");
	}
	
	public int getVotosTotais() {
		return eleitoresVotantes.size();
	}
	
	public boolean candidatoValido(Candidato c) {
		if (this.candidatos.contains(c))
			return true;
		else
			return false;
	}
	
	public boolean conferirDados(Eleitor e) {
		dao.salvar(logs, "Dados do eleitor " + e.Individuo.consultarCPF() + " recebidos");
		if (this.urna.getSessao() == e.getSessao() && this.urna.getZona() == e.getZona()) {
			if (this.eleitoresAptos.contains(e) && !this.eleitoresVotantes.contains(e))
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	public Voto iniciarVoto(char[] pwd) {
		if (this.mesario.conferirSenha(pwd)) {
			dao.salvar(logs, "Novo voto sendo iniciado.");
			Voto v = new Voto(this, null, null, null);
			return v;
		}
		else
			return null;
	}
	
	public Voto votoParcial(Voto v, Candidato c, boolean brancoOuNulo) {
		dao.salvar(logs, "Etapa " + v.getStatus() + " sendo efetuada.");
		if (candidatoValido(c)) {
			switch(v.getStatus()) {
				case SENADOR1: v.setPrimeiroSenador(c, brancoOuNulo);
				case SENADOR2: v.setSegundoSenador(c, brancoOuNulo);
				case PRESIDENTE: v.setPresidente(c, brancoOuNulo);
				case FINALIZADO: finalizarVoto(v);
				default: break;
			}
		}
		return v;
	}
	
	public boolean finalizarVoto(Voto v) {
		if (v.getStatus() == Status.FINALIZADO) {
			this.urna.registrarVoto(v);
			return true;
		}
		return false;
	}
}
