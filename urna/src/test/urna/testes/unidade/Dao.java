package urna.testes.unidade;

import java.util.List;
import java.util.Scanner;

import urna.model.dao.*;

public class Dao {
	
	private static final String path = System.getProperty("user.dir") + "/registros/";
	private static String arquivoTexto = "logs.txt";
	private static String arquivoCsv = "candidatos.csv";
	//private static String busca = "DAO*";
	
	private static String[] linha = new String[]{"Chico", "�urea", "Boulos"};
	
	private static String frase = "Teste unitario do DAO generico";
	
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in); 
		
		Generic dao = new Generic(path);
		if (dao.novoRegistro(arquivoTexto))
			System.out.println("Arquivo criado corretamente");
		dao.novoRegistro(arquivoCsv);
		
		dao.salvar(arquivoTexto, frase);
		System.out.println(dao.ler(arquivoTexto));
		System.out.println("");
		//System.out.println(dao.ler(arquivoTexto, busca));
		
		dao.escreverCsv(arquivoCsv, linha);
		List<String[]> registros = dao.lerCsv(arquivoCsv);
		registros.forEach(r -> {
			for (int j = 0; j < r.length; j++)
				System.out.print(r[j] + " ");
			System.out.println("");
		});
		
		dao.deletarRegistro(arquivoTexto);
		sc.close();
	}
}
