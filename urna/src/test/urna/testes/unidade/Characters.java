package urna.testes.unidade;

import static org.junit.Assert.*;
import org.junit.Test;

import urna.model.characters.*;

public class Characters {
	
	private static String nome = "Alceu Valenca";
	private static int[] cpf = {1, 3, 2, 4, 3, 5, 4, 6, 5, 7};
	private static int[] telefone = {9, 9, 9, 6, 6, 1, 2, 3, 4};
	private static String endereco = "Praia de Boa Viagem";	
	private static String pwd = "Teste";
	
	private static String novoEndereco = "Olinda";
	private static int[] novoTelefone = {8, 9, 6, 5, 1, 3, 2, 5, 5};
	
	private static int numCandidato = 50;
	private static String partidoCandidato = "PSOL";
	private static String pais = "Brasil";

	private static int[] regEleitoral = {1, 2, 3, 4, 5, 6, 7};
	private static int zonaEleitoral = 42;
	private static int sessaoEleitoral = 666;

	Individuo individuo = new Individuo(nome, cpf, telefone, endereco);
	Mesario mesario = new Mesario(ind, pwd.toCharArray());
	Candidato candidato = new Candidato(ind, numCandidato, partidoCandidato, Cargo.PRESIDENTE, pais);
	Eleitor eleitor = new Eleitor(1, ind, regEleitoral, sessaoEleitoral, zonaEleitoral);

	@Test
	public void testaIndividuo() {		
		assertEquals(individuo.consultarCPF(), cpf);
		assertTrue(!individuo.atualizarEndereco(null));
		assertTrue(!individuo.atualizarEndereco(""));
		assertTrue(!individuo.atualizarEndereco(novoEndereco));
		assertTrue(!individuo.atualizarTelefone(null));
		assertTrue(!individuo.atualizarTelefone(new int[]{2, 3, 4, 5, 8, 3}));
		asserTrue(individuo.atualizarTelefone(novoTelefone));
	}

	@Test
	public void testaMesario() {		
		assertEquals(mesario.individuo.consultarCPF(),cpf);
		assertTrue(mesario.conferirSenha(pwd.toCharArray()));
	}

	@Test
	public void testaCandidato() {		
		assertEquals(candidato.getCargo(), Cargo.PRESIDENTE);
		assertEquals(candidato.getNumero(), numCandidato);
		assertEquals(candidato.getLocalidade(), pais);
		assertEquals(candidato.getPartido(), partidoCandidato);
	}

	@Test
	public void testaEleitor() {		
		assertEquals(eleitor.getRegistro(), regEleitoral);
		assertEquals(eleitor.getSessao(), sessaoEleitoral);
		assertEquals(eleitor.getZona(), zonaEleitoral);
	}
		
}
